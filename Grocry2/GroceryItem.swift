//
//  GroceryItem.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

final class GroceryItem {

    var groceryItemId :NSNumber
    var title :String
    var shoppingList :ShoppingList
    var datePurchased :NSDate
    var dateConsumed :NSDate
    
    init() {
        self.groceryItemId = NSNumber(integer: 0)
        self.title = ""
        self.shoppingList = ShoppingList()
        self.datePurchased = NSDate()
        self.dateConsumed = NSDate()
    }
}

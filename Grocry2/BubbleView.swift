//
//  BubbleView.swift
//  GT
//
//  Created by Mohammad AZam on 9/17/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

public class BubbleView: UIView {

    var countLabel :UILabel = UILabel()
    
    func setup() {
        
        self.layer.cornerRadius = 6.0;
        self.countLabel = UILabel()
        self.countLabel.font = UIFont(name: "Gill Sans", size: 14)
        self.countLabel.textColor = UIColor.whiteColor()
        self.countLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(countLabel)
        
        applyConstraints()
    }
    
    func applyConstraints() {
       
        let constraintY = NSLayoutConstraint(item: self.countLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0.0)
        
        let centerConstraintX = NSLayoutConstraint(item: self.countLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0)
        
        self.addConstraint(constraintY)
        self.addConstraint(centerConstraintX)
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
       
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

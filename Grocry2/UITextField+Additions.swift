//
//  UITextField+Extensions.swift
//  GroceriesTracker
//
//  Created by Mohammad AZam on 9/10/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

extension UITextField {

    public static func textBoxForAddNewItemView(placeHolderText :String) -> UITextField {
        
        let textbox = UITextField()
        textbox.placeholder = placeHolderText
        textbox.leftView = UIView(frame: CGRectMake(10, 0, 10, 0))
        textbox.leftViewMode = UITextFieldViewMode.Always
        textbox.font = UIFont(name: "Gill Sans", size: 17)
        return textbox
    }
    
}

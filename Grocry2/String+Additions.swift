//
//  String+Additions.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/3/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

extension String {

    func stringByAppendingPathComponent(pathComponent :String) -> String {
        return (self as NSString).stringByAppendingPathComponent(pathComponent)
    }
}

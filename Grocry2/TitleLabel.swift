//
//  TitleLabel.swift
//  Grocry2
//
//  Created by Mohammad AZam on 2/21/16.
//  Copyright © 2016 Mohammad Azam. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        
        self.font = UIFont.fontForTitle()
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

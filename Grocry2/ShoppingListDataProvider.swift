//
//  ShoppingListDataProvider.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit


protocol ShoppingListDataProviderDelegate {
    
    func dataProviderdidInsertRowAtIndexPath(indexPath :NSIndexPath)
    func dataProviderdidDeleteRowAtIndexPath(indexPath :NSIndexPath)
}

class ShoppingListDataProvider: NSObject {

    private let grocryService :GrocryService!
    var delegate :ShoppingListDataProviderDelegate!
    
    var shoppingLists = Array<ShoppingList>()
    
    override init() {
        
        self.grocryService = GrocryService()
        shoppingLists = self.grocryService.getAllShoppingLists()
        super.init()
    }
    
    func saveShoppingList(shoppingList :ShoppingList)  {
        
        self.grocryService.saveShoppingList(shoppingList)
        self.shoppingLists.insert(shoppingList, atIndex: 0)
        self.delegate.dataProviderdidInsertRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
    }
    
    func deleteShoppingList(shoppingList :ShoppingList,indexPath :NSIndexPath) {
        
        self.grocryService.deleteShoppingList(shoppingList)
        self.shoppingLists.removeAtIndex(indexPath.row)
        self.delegate.dataProviderdidDeleteRowAtIndexPath(indexPath)
    }
    
    func objectAtIndexPath(indexPath :NSIndexPath) -> ShoppingList {
        
        return self.shoppingLists[indexPath.row]
    }
    
}

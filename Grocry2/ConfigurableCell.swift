//
//  ConfigurableCell.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

protocol ConfigurableCell {

    typealias DataSource
    func configureForObject(object :DataSource)
}


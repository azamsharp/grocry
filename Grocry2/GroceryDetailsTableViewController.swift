//
//  GroceryDetailsTableViewController.swift
//  Grocry2
//
//  Created by Mohammad AZam on 2/20/16.
//  Copyright © 2016 Mohammad Azam. All rights reserved.
//

import UIKit

class GroceryDetailsTableViewController: UITableViewController {

    var groceryItem :GroceryItem!
    @IBOutlet var titleTextField :UITextField!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        setup();
    }

    func setup() {
        
        self.title = groceryItem.title
        self.titleTextField.placeholder = self.title
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

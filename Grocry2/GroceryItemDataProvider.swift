//
//  GroceryItemDataProvider.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/5/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

protocol GroceryItemDataProviderDelegate {
    
    func dataProviderdidInsertRowAtIndexPath(indexPath :NSIndexPath)
    func dataProviderdidDeleteRowAtIndexPath(indexPath :NSIndexPath) 
}

class GroceryItemDataProvider: NSObject {

    private let grocryService :GrocryService!
    var delegate :GroceryItemDataProviderDelegate!
    
    var groceryItems = Array<GroceryItem>()
    
    init(shoppingList :ShoppingList) {
        
        self.grocryService = GrocryService()
        groceryItems = self.grocryService.getGroceryItemsByShoppingList(shoppingList)
        super.init()
    }
    
    func saveGroceryItem(groceryItem :GroceryItem)  {
        
        self.grocryService.saveGroceryItem(groceryItem)
        self.groceryItems.insert(groceryItem, atIndex: 0)
        self.delegate.dataProviderdidInsertRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0))
    }
    
    func moveGroceryItemToPantry(groceryItem :GroceryItem, indexPath :NSIndexPath) {
        
       let movedToPantry = self.grocryService.moveGroceryItemToPantry(groceryItem)
        if(movedToPantry) {
            // remove from the grocery list 
            // TODO
        }
        
    }
    
    func deleteGroceryItem(groceryItem :GroceryItem,indexPath :NSIndexPath) {
        
        self.grocryService.deleteGroceryItem(groceryItem)
        self.groceryItems.removeAtIndex(indexPath.row)
        self.delegate.dataProviderdidDeleteRowAtIndexPath(indexPath)
    }
    
    func objectAtIndexPath(indexPath :NSIndexPath) -> GroceryItem {
        
        return self.groceryItems[indexPath.row]
    }

    
}

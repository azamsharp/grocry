//
//  ShoppingListTableViewCell.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/3/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel :UILabel!
    @IBOutlet var bubbleCountView :BubbleView!
}

extension ShoppingListTableViewCell : ConfigurableCell {
    
    func configureForObject(shoppingList: ShoppingList) {
        
        self.titleLabel.text = shoppingList.title
        self.bubbleCountView.countLabel.text = "\(shoppingList.groceryItems.count)"
        self.bubbleCountView.backgroundColor = UIColor(fromHexString: "1BE542")
        self.bubbleCountView.sizeToFit()
    }
}

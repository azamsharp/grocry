//
//  ShoppingList.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/3/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

final class ShoppingList {
    
    var shoppingListId :NSNumber
    var title :String
    var groceryItems :[GroceryItem]
    
    init() {
        self.title = ""
        self.groceryItems = [GroceryItem]()
        self.shoppingListId = NSNumber(integer: 0)
    }
    
}

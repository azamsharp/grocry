//
//  ShoppingListTableViewController.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/3/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class ShoppingListTableViewController: UITableViewController, SegueHandlerType {

    enum SegueIdentifier: String {
        case ShowGroceryItems = "showGroceryItems"
    }
    
    var shoppingLists = [ShoppingList]()
    
    var dataSource : ShoppingListDataSource<ShoppingListTableViewCell,ShoppingListDataProvider>!
    var dataProvider :ShoppingListDataProvider!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // refresh the tableview on every view appear event
        setupTableView()
    }
    
    func setupTableView() {
        
        self.dataProvider = ShoppingListDataProvider()
        
        self.dataSource = ShoppingListDataSource(cellIdentifier :"ShoppingListTableViewCell",dataProvider:dataProvider,tableView: self.tableView)
        
        self.tableView.dataSource = self.dataSource
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNewShoppingList(title :String) {
        
        let shoppingList = ShoppingList()
        shoppingList.title = title
        
        self.dataProvider.saveShoppingList(shoppingList)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segueIdentifierForSegue(segue) {
           
        case .ShowGroceryItems:
            
                guard let vc = segue.destinationViewController as? GroceriesTableViewController else {
                    fatalError("Segue with identifier \(segue.identifier) not found!")
                }
                
                guard let indexPath = self.tableView.indexPathForSelectedRow else {
                    fatalError("The index path is not defined!")
                }
                
                vc.shoppingList = self.dataProvider.objectAtIndexPath(indexPath)
        }
    }
   
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let addNewItemView = AddNewItemView(placeHolderText: "Add new shopping list", delegate: self) { (title: String) -> Void in
            
            self.addNewShoppingList(title)
        }
        
        return addNewItemView
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

}

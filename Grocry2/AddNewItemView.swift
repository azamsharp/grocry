//
//  AddNewItemView.swift
//  GroceriesTracker
//
//  Created by Mohammad AZam on 9/10/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit


class AddNewItemView: UIView,UITextFieldDelegate {

    var placeHolderText :String?
    var delegate :UIViewController?
    var callback :((String) -> Void)!
    
    init(placeHolderText :String, delegate :UIViewController, callback :(String) -> Void) {
        
        self.placeHolderText = placeHolderText
        self.delegate = delegate
        self.callback = callback
        
        super.init(frame: CGRectMake(0, 0, delegate.view.frame.size.width, 44))
        setup()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
     
        if let text = textField.text {
            textField.text = ""
            self.callback(text)
        }
        
        return textField.resignFirstResponder()
    }
    
    private func setup() {
        
        self.backgroundColor = UIColor(fromHexString: "FAFAFA")
        let stackView = UIStackView(frame: self.frame)
        let textbox = UITextField.textBoxForAddNewItemView(self.placeHolderText!)
        textbox.delegate = self
        textbox.frame = self.frame
        
        stackView.addSubview(textbox)
        self.addSubview(stackView)
    }
    
    required init(coder :NSCoder) {
        
        super.init(coder: coder)!
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}

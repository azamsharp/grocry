//
//  GrocryService.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/3/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class GrocryService: NSObject {

    var db :FMDatabase!
    
    override init() {
        
        guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else {
            fatalError("AppDelegate not defined!")
        }
        
        db = FMDatabase(path: appDelegate.databasePath)
        db.logsErrors = true
        super.init()
    }
    
    func getGroceryItemsByShoppingList(shoppingList :ShoppingList) -> [GroceryItem] {
        
        var groceryItems = [GroceryItem]()
        
        db.open()
        
        let results = db.executeQuery("SELECT groceryItemId, title FROM GroceryItems WHERE shoppingListId = ?", withArgumentsInArray: [shoppingList.shoppingListId])
        
        while(results.next()) {
            
            var groceryItem = GroceryItem()
            groceryItem.groceryItemId = NSNumber(int :results.intForColumn("groceryItemId"))
            groceryItem.title = results.stringForColumn("title")
            
            groceryItems.append(groceryItem)
        }
        
        defer {

        results.close()
        db.close()
            
        }
        
        return groceryItems
    }
    
    func deleteShoppingList(shoppingList :ShoppingList) {
        
        db.open()
        
        let success = db.executeUpdate("DELETE FROM ShoppingLists WHERE shoppingListId = ?", withArgumentsInArray: [shoppingList.shoppingListId])

        defer {
            db.close()
        }
    }
    
    func deleteGroceryItem(groceryItem :GroceryItem) {
        
        db.open()
        
        db.executeUpdate("DELETE FROM GroceryItems WHERE groceryItemId = ?", withArgumentsInArray: [groceryItem.groceryItemId])
        
        defer {
            db.close()
        }
    }
    
    func moveGroceryItemToPantry(groceryItem :GroceryItem) -> Bool {
        
        var result = false
        
        db.open()
        
        result = db.executeUpdate("INSERT INTO PantryItems(groceryItemId,datePurchased)", withArgumentsInArray: [groceryItem.groceryItemId,groceryItem.datePurchased])
        
        defer {
        db.close()
        }
        
        return result
        
    }
    
    func getAllShoppingLists() -> [ShoppingList] {
        
        var shoppingLists = [ShoppingList]()
        
        db.open()
        
        let results = db.executeQuery("SELECT shoppingListId, title FROM ShoppingLists", withArgumentsInArray: [])
        
        while(results.next()) {

            let shoppingList = ShoppingList()
            
            shoppingList.shoppingListId = NSNumber(int: results.intForColumn("shoppingListId"))
            shoppingList.title = results.stringForColumn("title")
            shoppingLists.append(shoppingList)
        }
        
        results.close()
        db.close()

        // get the grocery items in each shopping list
        for shoppingList in shoppingLists {
            shoppingList.groceryItems = getGroceryItemsByShoppingList(shoppingList)
        }
        
        return shoppingLists
    }
    
    func saveGroceryItem(groceryItem :GroceryItem) -> Bool {
        
        db.open()
        
        let success = db.executeUpdate("INSERT INTO GroceryItems(title,shoppingListId) VALUES(?,?)", withArgumentsInArray: [groceryItem.title, groceryItem.shoppingList.shoppingListId])
        
        
        defer {
           db.close()
        }
        
        return success
    }
    
    func saveShoppingList(var shoppingList :ShoppingList) {

        db.open()
        
        db.executeUpdate("INSERT INTO ShoppingLists(title) VALUES(?)", withArgumentsInArray: [shoppingList.title])
        
        shoppingList.shoppingListId = NSNumber(longLong: db.lastInsertRowId())
        
        defer {
        db.close()
        }
        
    }
    
}

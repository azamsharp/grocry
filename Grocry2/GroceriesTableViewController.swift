//
//  GroceriesTableViewController.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class GroceriesTableViewController: UITableViewController, SegueHandlerType {

    var shoppingList :ShoppingList!
    var dataProvider :GroceryItemDataProvider!
    var dataSource : GroceryItemDataSource<GroceryItemTableViewCell,GroceryItemDataProvider>!

    enum SegueIdentifier :String {
        case ShowGroceryDetails = "showGroceryDetails"
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {

        self.dataProvider = GroceryItemDataProvider(shoppingList: self.shoppingList)
        self.dataSource = GroceryItemDataSource(cellIdentifier :"GroceryItemTableViewCell",dataProvider:self.dataProvider,tableView: self.tableView)
        
        self.tableView.dataSource = self.dataSource
        self.tableView.sectionHeaderHeight = 44
        self.title = self.shoppingList.title
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNewGroceryItem(title :String) {
       
        let groceryItem = GroceryItem()
        groceryItem.title = title
        groceryItem.shoppingList = self.shoppingList
        
        self.dataProvider.saveGroceryItem(groceryItem)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        switch segueIdentifierForSegue(segue) {
            case .ShowGroceryDetails:
            
                guard let vc = segue.destinationViewController as? GroceryDetailsTableViewController else {
                    fatalError("Segue Identifier \(segue.identifier) not found")
                }
            
                guard let indexPath = self.tableView.indexPathForSelectedRow else {
                    fatalError("index path is not defined")
                }
            
                vc.groceryItem = self.dataProvider.objectAtIndexPath(indexPath)
        }
        
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let addNewItemView = AddNewItemView(placeHolderText: "Add new grocery item", delegate: self) { (title: String) -> Void in
            
            self.addNewGroceryItem(title)
            
        }
        
        return addNewItemView
    }
}

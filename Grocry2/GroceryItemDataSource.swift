//
//  GroceryItemDataSource.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/5/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class GroceryItemDataSource<Cell :GroceryItemTableViewCell, Data:GroceryItemDataProvider where Cell:ConfigurableCell, Cell.DataSource == GroceryItem>  : NSObject,UITableViewDataSource, GroceryItemDataProviderDelegate,GroceryItemTableViewCellDelegate {

    private let cellIdentifier :String!
    private let dataProvider :GroceryItemDataProvider!
    private let tableView :UITableView!
    
    init(cellIdentifier :String!,dataProvider :GroceryItemDataProvider, tableView :UITableView) {
        
        self.cellIdentifier = cellIdentifier
        self.dataProvider = dataProvider
        self.tableView = tableView
        
        super.init()
        
        self.dataProvider.delegate = self
    }
    
    func groceryItemTableViewCellMoveToPantry(cell: UITableViewCell) {
       
        guard let indexPath = self.tableView.indexPathForCell(cell) else {
            fatalError("")
        }
        
        let groceryItem = self.dataProvider.objectAtIndexPath(indexPath)
        self.dataProvider.moveGroceryItemToPantry(groceryItem,indexPath: indexPath)
    }
    
    func groceryItemTableViewCellDeleteItem(cell: UITableViewCell) {
        
        // get the index
        guard let indexPath = self.tableView.indexPathForCell(cell) else {
            fatalError("IndexPath not found")
        }
        
        let groceryItem = self.dataProvider.objectAtIndexPath(indexPath)
        self.dataProvider.deleteGroceryItem(groceryItem, indexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier, forIndexPath: indexPath) as? Cell else {
            fatalError("Cell with identifier \(self.cellIdentifier) is not defined!")
        }
        
        cell.groceryItemTableViewCellDelegate = self 
        
        let groceryItem = self.dataProvider.objectAtIndexPath(indexPath)
        cell.configureForObject(groceryItem)
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            
            // get the object
            let groceryItem = self.dataProvider.objectAtIndexPath(indexPath)
            self.dataProvider.deleteGroceryItem(groceryItem,indexPath :indexPath)
            
        }
    }
    
    func dataProviderdidInsertRowAtIndexPath(indexPath: NSIndexPath) {
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func dataProviderdidDeleteRowAtIndexPath(indexPath: NSIndexPath) {
        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            
        return dataProvider.groceryItems.count
    }
}

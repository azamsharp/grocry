//
//  SegueHandlerType.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

public protocol SegueHandlerType {
    
    typealias SegueIdentifier :RawRepresentable
}

extension SegueHandlerType where Self:UIViewController, SegueIdentifier.RawValue == String {
    
    public func segueIdentifierForSegue(segue :UIStoryboardSegue) -> SegueIdentifier {
        
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier)
            else {
                fatalError("Unknown Segue: \(segue)")
        }
        
        return segueIdentifier
    }
}
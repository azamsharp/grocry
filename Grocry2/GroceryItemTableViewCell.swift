//
//  GroceryItemTableViewCell.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

protocol GroceryItemTableViewCellDelegate {
    
    func groceryItemTableViewCellMoveToPantry(cell :UITableViewCell)
    func groceryItemTableViewCellDeleteItem(cell :UITableViewCell)
}

class GroceryItemTableViewCell: MGSwipeTableCell {

    @IBOutlet var titleLabel :UILabel!
    var groceryItemTableViewCellDelegate :GroceryItemTableViewCellDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        
        // setup swipe buttons
        setupSwipeButtons()
    }
   
}

extension GroceryItemTableViewCell : ConfigurableCell {
    
    func configureForObject(groceryItem :GroceryItem) {
        
        self.titleLabel.text = groceryItem.title 
    }
    
    func setupSwipeButtons() {
        
        // add swipe button to the cell
        let moveToPantryButton = MGSwipeButton(title: "Move to Pantry", backgroundColor: UIColor(fromHexString: "1BE542")) { (sender :MGSwipeTableCell!) -> Bool in
            
            self.groceryItemTableViewCellDelegate.groceryItemTableViewCellMoveToPantry(sender)
            
            return true
        }
        
        // add the swipe right to left delete button
        let deleteGroceryItemButton = MGSwipeButton(title: "Delete", backgroundColor: UIColor.redColor()) { (sender :MGSwipeTableCell!) -> Bool in
            
            self.groceryItemTableViewCellDelegate.groceryItemTableViewCellDeleteItem(sender)
            
            return true
        }
        
        moveToPantryButton.titleLabel?.font = UIFont.fontForTitle()
        deleteGroceryItemButton.titleLabel?.font = UIFont.fontForTitle()
        
        self.leftButtons = [moveToPantryButton]
        self.rightButtons = [deleteGroceryItemButton]
        
        self.leftExpansion.buttonIndex = 0
        self.rightExpansion.buttonIndex = 0
    }
    
}

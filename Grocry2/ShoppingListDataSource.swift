//
//  ShoppingListDataSource.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/4/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

class ShoppingListDataSource<Cell :UITableViewCell,Data:ShoppingListDataProvider where Cell :ConfigurableCell, Cell.DataSource == ShoppingList>: NSObject,UITableViewDataSource,ShoppingListDataProviderDelegate {
    
    private let cellIdentifier :String!
    private let dataProvider :ShoppingListDataProvider!
    private let tableView :UITableView!
    
    init(cellIdentifier :String!,dataProvider :ShoppingListDataProvider, tableView :UITableView) {
        
        self.cellIdentifier = cellIdentifier
        self.dataProvider = dataProvider
        self.tableView = tableView

        super.init()
        
        self.dataProvider.delegate = self
    }
    
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            
            // get the object 
            let shoppingList = self.dataProvider.objectAtIndexPath(indexPath)
            self.dataProvider.deleteShoppingList(shoppingList,indexPath :indexPath)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier, forIndexPath: indexPath) as? Cell else {
            fatalError("Cell with identifier \(self.cellIdentifier) is not defined!")
        }
        
        
        let shoppingList = self.dataProvider.objectAtIndexPath(indexPath)
        cell.configureForObject(shoppingList)
        
        return cell
    }
    
    func dataProviderdidDeleteRowAtIndexPath(indexPath: NSIndexPath) {
        self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func dataProviderdidInsertRowAtIndexPath(indexPath: NSIndexPath) {
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            
        return dataProvider.shoppingLists.count
    }

    
}

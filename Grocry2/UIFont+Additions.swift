//
//  UIFont+Additions.swift
//  Grocry2
//
//  Created by Mohammad AZam on 11/28/15.
//  Copyright © 2015 Mohammad Azam. All rights reserved.
//

import UIKit

extension UIFont {

    static func fontForTitle() -> UIFont {
        
        guard let font = UIFont(name: "Gill Sans", size: 17) else {
            fatalError("Font not available")
        }
        return font
    }
    
}
